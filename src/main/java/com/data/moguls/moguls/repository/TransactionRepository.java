package com.data.moguls.moguls.repository;

import com.data.moguls.moguls.model.OfferConfig;
import com.data.moguls.moguls.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    // and oc.payment_mode like '%:paymentMode%' and t.added_on between :validFrom and :validTo
    @Query(value = "select oc.id from transaction t join offer_config oc on t.offer_id = oc.id where t.transaction_status='success' and t.offer_status='success' and oc.payment_mode like %:paymentMode%  and t.added_on between :validFrom and :validTo group by t.offer_id order by sum(t.discount) desc limit 1",nativeQuery = true)
    Integer findHighestGmvOffer(String paymentMode,LocalDate validFrom,LocalDate validTo);
}

