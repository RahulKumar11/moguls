package com.data.moguls.moguls.repository;


import com.data.moguls.moguls.model.OfferConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferConfigRepository extends JpaRepository<OfferConfig, Integer> , JpaSpecificationExecutor<OfferConfig> {
}
