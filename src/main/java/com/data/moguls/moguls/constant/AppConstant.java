package com.data.moguls.moguls.constant;

public class AppConstant {
    public static final String CREATE_OFFER_SPECS="based on information provided in the technical specifications delimited by triple backticks Format your response as a JSON object with discount, offerType, validFrom, validTo, discountType, minTxnAmount, maxTxnAmount, paymentMode, paymentCode, title, sku as keys. . Technical specifications: ```possible offerType values are instant,cashback and read discount as float value and possible discountType values are absolute, percentage and read paymentMode as comma seperated string and read paymentCode as comma seperated string and read sku as name of the product mentioned in prompt which is comma seperated string if there is no product in prompt read skus as null";
    public static final String GENERATE_TYPE_SPECS="summarize the technical specifications delimited by triple backticks into one word where possible values are 'create','find','sqlquery','clone' . . Technical specifications:```";

    public static final String CLONE_OFFER_SPECS="based on technical specification delimited by triple backticks Format your response as a JSON object with paymentMode, validFrom, validTo as keys . Technical specifications:```values of paymentMode can be creditcard and debitcard if there is no paymentMode in prompt read paymentMode as null if there is no validFrom in prompt read it as null and if there is no validTo in prompt read it as null```";
    public static final String SYMBOL = "@";
}
