package com.data.moguls.moguls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MogulsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MogulsApplication.class, args);
	}

}
