package com.data.moguls.moguls.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class OfferResponse {
    private String message;

    private Integer id;
    private String offerKey;
    private String title;
    private String offerType;
    private String discountType;
    private BigDecimal discount;
    //@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String validFrom;
    //@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String validTo;
    private BigDecimal minTxnAmount;
    private BigDecimal maxTxnAmount;
    private Integer count;
    private String paymentMode;
    private String paymentCode;
    private Integer merchantId;
    private String sku;
    private String userToken;
}
