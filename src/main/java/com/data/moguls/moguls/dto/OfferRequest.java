package com.data.moguls.moguls.dto;

import lombok.Data;

@Data
public class OfferRequest {
    private String message;
    private Integer merchantId;
}
