package com.data.moguls.moguls.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class FetchOfferParam {
    private String offerType;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
    private BigDecimal minTxnAmount;
    private String paymentMode;
    private String paymentCode;
    private Integer merchantId;
    private String merchantName;
    private String skus;
}
