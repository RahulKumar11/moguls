package com.data.moguls.moguls.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CloneDto {
    private  String paymentMode;
    private LocalDate validFrom;
    private LocalDate validTo;

}
