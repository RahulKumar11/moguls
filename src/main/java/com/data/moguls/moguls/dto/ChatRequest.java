package com.data.moguls.moguls.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChatRequest {

    private String model;
    private List<Message> messages;


    public ChatRequest(String model, String prompt) {
        this.model = model;
       //String spec="generate a offerKey as random string, offerId as random 4 digit integer and read discount as float value from prompt and read offerType as string whether it is instant or cashback from prompt and read validFrom as date from prompt if validFrom is not given in prompt take current date and if validFrom is in past then do not create message  if validTo is not given in prompt then take validTo as nextday of validFrom";
        this.messages = new ArrayList<>();
        this.messages.add(new Message("user", prompt));
    }

}

