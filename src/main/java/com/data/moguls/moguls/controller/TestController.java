package com.data.moguls.moguls.controller;

import com.data.moguls.moguls.dto.ChatRequest;
import com.data.moguls.moguls.dto.ChatResponse;
import com.data.moguls.moguls.dto.OfferRequest;
import com.data.moguls.moguls.dto.OfferResponse;
import com.data.moguls.moguls.model.Transaction;
import com.data.moguls.moguls.service.OfferService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
@RequestMapping("api/v1")
public class TestController {

    @Qualifier("openaiRestTemplate")
    @Autowired
    private RestTemplate restTemplate;

    @Value("${openai.model}")
    private String model;

    @Value("${openai.api.url}")
    private String apiUrl;

    @Autowired
    private OfferService offerService;


//    @GetMapping("/test")
//    public String test(){
//        return "hello world ";
//    }


    @GetMapping("/chat")
    public String chat(@RequestParam String prompt) {
        ChatRequest request = new ChatRequest(model, prompt);
        ChatResponse response = null;
        try {
            response = restTemplate.postForObject(apiUrl, request, ChatResponse.class);
            if (response == null || response.getChoices() == null || response.getChoices().isEmpty()) {
                return "No response";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.getChoices().get(0).getMessage().getContent();
    }

    @PostMapping("/offer")
    public OfferResponse offer(@RequestBody OfferRequest offerRequest) throws JsonProcessingException {
          return offerService.offer(offerRequest);
    }

    @PostMapping("/transaction")
    public void offer(@RequestBody Transaction transaction) {
        offerService.createTransaction(transaction);
    }


}
