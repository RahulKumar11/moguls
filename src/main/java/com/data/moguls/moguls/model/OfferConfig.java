package com.data.moguls.moguls.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
@Data
@Entity
public class OfferConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String offerKey;
    private String title;
    private String offerType;
    private String discountType;
    private BigDecimal discount;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate validFrom;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate validTo;
    private BigDecimal minTxnAmount;
    private BigDecimal maxTxnAmount;
    private Integer count;
    private String paymentMode;
    private String paymentCode;
    private Integer merchantId;
    private String sku;
    private String userToken;
}
