package com.data.moguls.moguls.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer offerId;
    private  Integer paymentId;
    private String offerKey;
    private String userToken;
    private BigDecimal amount;
    private  BigDecimal discount;
    private String sku;
    private  Integer merchantId;
    private  String transactionStatus; //capture/failed
    private  String offerStatus; //
    //@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
   // @Column(name = "added_on")
    //private LocalDate addedOn;
}



