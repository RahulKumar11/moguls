package com.data.moguls.moguls.service;

import com.data.moguls.moguls.constant.AppConstant;
import com.data.moguls.moguls.dto.ChatRequest;
import com.data.moguls.moguls.dto.ChatResponse;
import com.data.moguls.moguls.dto.CloneDto;
import com.data.moguls.moguls.dto.FetchOfferParam;
import com.data.moguls.moguls.dto.OfferRequest;
import com.data.moguls.moguls.dto.OfferResponse;
import com.data.moguls.moguls.model.OfferConfig;
import com.data.moguls.moguls.model.Transaction;
import com.data.moguls.moguls.repository.OfferConfigRepository;
import com.data.moguls.moguls.repository.TransactionRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.data.moguls.moguls.constant.AppConstant.CLONE_OFFER_SPECS;
import static com.data.moguls.moguls.constant.AppConstant.CREATE_OFFER_SPECS;
import static com.data.moguls.moguls.constant.AppConstant.GENERATE_TYPE_SPECS;

@Service
public class OfferService {

    @Qualifier("openaiRestTemplate")
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OfferConfigRepository offerConfigRepository;

    @Autowired
    private TransactionRepository transactionRepository;


    @Value("${openai.model}")
    private String model;

    @Value("${openai.api.url}")
    private String apiUrl;


    public OfferResponse offer(OfferRequest offerRequest) throws JsonProcessingException {
        String jsonString=call(GENERATE_TYPE_SPECS+offerRequest.getMessage()+"```");
        //JsonObject messageObject = JsonParser.parseString(jsonObject).getAsJsonObject();
        OfferResponse offerResponse=null;
        if(jsonString.equals("create")){
            String jsonStringForCreateOffer=call(offerRequest.getMessage()+CREATE_OFFER_SPECS);
            offerResponse=createOffer(offerRequest.getMerchantId(),jsonStringForCreateOffer);
        } else if (jsonString.equals("clone")) {
            String jsonStringForCloneOffer=call(offerRequest.getMessage()+CLONE_OFFER_SPECS);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.findAndRegisterModules();
            CloneDto cloneDto = mapper.readValue(jsonStringForCloneOffer, CloneDto.class);

            Integer offerId=transactionRepository.findHighestGmvOffer(cloneDto.getPaymentMode(), cloneDto.getValidFrom(),cloneDto.getValidTo());
                Optional<OfferConfig> offerConfigOptional=offerConfigRepository.findById(offerId);
                if(offerConfigOptional.isPresent()){
                 OfferConfig newOffer=getClonedOffer(offerConfigOptional.get());
                    newOffer=offerConfigRepository.save(newOffer);
                     offerResponse=getResponse(newOffer);
                    offerResponse.setMessage("Offer is cloned  !!!");
                }
        }
        return offerResponse;
    }

    private OfferConfig getClonedOffer(OfferConfig offerConfig) {
        OfferConfig offerConfig1=new OfferConfig();
        offerConfig1.setOfferKey(getOfferKey(offerConfig.getTitle()));
        offerConfig1.setOfferType(offerConfig.getOfferType());
        offerConfig1.setCount(offerConfig.getCount());
        offerConfig1.setDiscount(offerConfig.getDiscount());
        offerConfig1.setDiscountType(offerConfig.getDiscountType());
        offerConfig1.setMerchantId(offerConfig.getMerchantId());
        offerConfig1.setMaxTxnAmount(offerConfig.getMaxTxnAmount());
        offerConfig1.setPaymentCode(offerConfig.getPaymentCode());
        offerConfig1.setMinTxnAmount(offerConfig.getMinTxnAmount());
        offerConfig1.setTitle(offerConfig.getTitle());
        offerConfig1.setUserToken(offerConfig.getUserToken());
        offerConfig1.setValidFrom(offerConfig.getValidFrom());
        offerConfig1.setValidTo(offerConfig.getValidTo());
        offerConfig1.setSku(offerConfig.getSku());
        offerConfig1.setPaymentMode(offerConfig.getPaymentMode());
        return offerConfig1;
    }

    private void fetchOffer(String jsonObject) {
        FetchOfferParam fetchOfferParam=new FetchOfferParam();
        BeanUtils.copyProperties(jsonObject,fetchOfferParam);
   //     offerConfigRepository.findAll(OfferSpecification.fetchOffer(fetchOfferParam));
    }

    private OfferResponse createOffer(Integer merchantId, String messageObject) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.findAndRegisterModules();

        OfferConfig offerConfig = mapper.readValue(messageObject, OfferConfig.class);
        offerConfig.setMerchantId(merchantId);
        offerConfig.setOfferKey(getOfferKey(offerConfig.getTitle()));
        offerConfig=offerConfigRepository.save(offerConfig);
        OfferResponse offerResponse=getResponse(offerConfig);
        offerResponse.setMessage(" Offer is created !!!");
        return offerResponse;
    }


    public String  call(String prompt){
        ChatRequest request = new ChatRequest(model, prompt);
        ChatResponse response = null;
        try {
            response = restTemplate.postForObject(apiUrl, request, ChatResponse.class);
            if (response == null || response.getChoices() == null || response.getChoices().isEmpty()) {
                return "No response";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.getChoices().get(0).getMessage().getContent();
    }

    public static String getOfferKey(String title) {
        if ("".equals(title)) {
            return title;
        }
        title = removeCharacters(title,  new HashSet<>(List.of("")));
        title = title.substring(0, Math.min(title.length(), 20));
        return title + AppConstant.SYMBOL + generateRandomId(12);
    }

    public static String generateRandomId(int numOfChar) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABSDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        SecureRandom r = new SecureRandom();
        char[] id = new char[numOfChar];
        for (int i = 0; i < numOfChar; i++) {
            id[i] = chars[r.nextInt(chars.length)];
        }
        return new String(id);
    }
    public static String removeCharacters(String title, Set<String> characterToRemove) {
        if (title == null) {
            return  null;
        }

        title = title.replaceAll("\\s", "");
        for (String s : characterToRemove) {
            title = title.replaceAll(s, "");
        }
        return title;
    }

    public void createTransaction(Transaction transaction) {
         transactionRepository.save(transaction);
    }


    public OfferResponse getResponse(OfferConfig offerConfig){
        OfferResponse offerResponse=new OfferResponse();
        offerResponse.setId(offerConfig.getId());
        offerResponse.setOfferKey(offerConfig.getOfferKey());
        offerResponse.setOfferType(offerConfig.getOfferType());
        offerResponse.setCount(offerConfig.getCount());
        offerResponse.setDiscount(offerConfig.getDiscount());
        offerResponse.setDiscountType(offerConfig.getDiscountType());
        offerResponse.setMerchantId(offerConfig.getMerchantId());
        offerResponse.setMaxTxnAmount(offerConfig.getMaxTxnAmount());
        offerResponse.setPaymentCode(offerConfig.getPaymentCode());
        offerResponse.setMinTxnAmount(offerConfig.getMinTxnAmount());
        offerResponse.setTitle(offerConfig.getTitle());
        offerResponse.setUserToken(offerConfig.getUserToken());
        offerResponse.setValidFrom(String.valueOf(offerConfig.getValidFrom()));
        offerResponse.setValidTo(String.valueOf(offerConfig.getValidTo()));
        offerResponse.setSku(offerConfig.getSku());
        offerResponse.setPaymentMode(offerConfig.getPaymentMode());
        return offerResponse;
    }
}
